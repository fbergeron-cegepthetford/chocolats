package ui.chocolats;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.TextView;

import affaire.chocolats.Chocolats;


public class Controlleur extends Activity implements View.OnClickListener {
    TextView tvNbChocolats;
    CheckedTextView ctvCaramilk;
    CheckedTextView ctvCoffee;
    CheckedTextView ctvKitKat;
    CheckedTextView ctvMars;
    CheckedTextView ctvHenry;
    CheckedTextView ctvSmarties;
    Button btnRAZ;
    Chocolats chocolats;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialiser();
    }

    private void initialiser() {
        tvNbChocolats = findViewById(R.id.tvNbChocolats);
        ctvCaramilk = this.findViewById(R.id.ctvCaramilk);
        ctvCaramilk.setOnClickListener(this);
        ctvCoffee = this.findViewById(R.id.ctvCoffee);
        ctvCoffee.setOnClickListener(this);
        ctvKitKat = this.findViewById(R.id.ctvKitKat);
        ctvKitKat.setOnClickListener(this);
        ctvMars = this.findViewById(R.id.ctvMars);
        ctvMars.setOnClickListener(this);
        ctvHenry = this.findViewById(R.id.ctvHenry);
        ctvHenry.setOnClickListener(this);
        ctvSmarties = this.findViewById(R.id.ctvSmarties);
        ctvSmarties.setOnClickListener(this);
        btnRAZ = findViewById(R.id.btnRAZ);
        btnRAZ.setOnClickListener(this);
        chocolats = new Chocolats();
        this.remiseAZero();

    }
    private void remiseAZero(){
        ctvCaramilk.setChecked(false);
        ctvCoffee.setChecked(false);
        ctvKitKat.setChecked(false);
        ctvMars.setChecked(false);
        ctvHenry.setChecked(false);
        ctvSmarties.setChecked(false);
        tvNbChocolats.setText("0");

    }


    public void onClick(View v) {
        int valeur;

        switch (v.getId()) {
            case R.id.ctvCaramilk:
                if(ctvCaramilk.isChecked()){
                    valeur = Integer.valueOf(tvNbChocolats.getText().toString());
                    tvNbChocolats.setText("" + chocolats.soustraire(valeur));
                    ctvCaramilk.setChecked(false);
                    break;
                }
                else{
                    valeur = Integer.valueOf(tvNbChocolats.getText().toString());
                    tvNbChocolats.setText("" + chocolats.ajouter(valeur));
                    ctvCaramilk.setChecked(true);
                    break;
                }
            case R.id.ctvCoffee:
                if(ctvCoffee.isChecked()){
                    valeur = Integer.valueOf(tvNbChocolats.getText().toString());
                    tvNbChocolats.setText("" + chocolats.soustraire(valeur));
                    ctvCoffee.setChecked(false);
                    break;
                }
                else{
                    valeur = Integer.valueOf(tvNbChocolats.getText().toString());
                    tvNbChocolats.setText("" + chocolats.ajouter(valeur));
                    ctvCoffee.setChecked(true);
                    break;
                }
            case R.id.ctvKitKat:
                if(ctvKitKat.isChecked()){
                    valeur = Integer.valueOf(tvNbChocolats.getText().toString());
                    tvNbChocolats.setText("" + chocolats.soustraire(valeur));
                    ctvKitKat.setChecked(false);
                    break;
                }
                else{
                    valeur = Integer.valueOf(tvNbChocolats.getText().toString());
                    tvNbChocolats.setText("" + chocolats.ajouter(valeur));
                    ctvKitKat.setChecked(true);
                    break;
                }
            case R.id.ctvMars:
                if(ctvMars.isChecked()){
                    valeur = Integer.valueOf(tvNbChocolats.getText().toString());
                    tvNbChocolats.setText("" + chocolats.soustraire(valeur));
                    ctvMars.setChecked(false);
                    break;
                }
                else{
                    valeur = Integer.valueOf(tvNbChocolats.getText().toString());
                    tvNbChocolats.setText("" + chocolats.ajouter(valeur));
                    ctvMars.setChecked(true);
                    break;
                }
            case R.id.ctvHenry:
                if(ctvHenry.isChecked()){
                    valeur = Integer.valueOf(tvNbChocolats.getText().toString());
                    tvNbChocolats.setText("" + chocolats.soustraire(valeur));
                    ctvHenry.setChecked(false);
                    break;
                }
                else{
                    valeur = Integer.valueOf(tvNbChocolats.getText().toString());
                    tvNbChocolats.setText("" + chocolats.ajouter(valeur));
                    ctvHenry.setChecked(true);
                    break;
                }
            case R.id.ctvSmarties:
                if(ctvSmarties.isChecked()){
                    valeur = Integer.valueOf(tvNbChocolats.getText().toString());
                    tvNbChocolats.setText("" + chocolats.soustraire(valeur));
                    ctvSmarties.setChecked(false);
                    break;
                }
                else{
                    valeur = Integer.valueOf(tvNbChocolats.getText().toString());
                    tvNbChocolats.setText("" + chocolats.ajouter(valeur));
                    ctvSmarties.setChecked(true);
                    break;
                }
            case R.id.btnRAZ:
                this.remiseAZero();
                break;
        }
    }
}
