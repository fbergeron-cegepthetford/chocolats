package affaire.chocolats;

public class Chocolats {

    private int valeur;

    public Chocolats(){
        valeur = 0;
    }

    public int getValeur() {
        return valeur;
    }

    public int reinitialiser(){
        return valeur=0;
    }

    public int ajouter(int valeur){
        return valeur+=1;
    }
    public int soustraire(int valeur){
        return valeur-=1;
    }
}
